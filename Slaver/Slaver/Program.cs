﻿using Humanlights.ConsoleHelper;
using Humanlights.Extensions;
using Slaver.Services;
using System;
using System.Threading.Tasks;

namespace Slaver
{
    public class Program
    {
        public static Humanlights.ConsoleHelper.ConsoleHandler ConsoleHandler { get; set; } = new Humanlights.ConsoleHelper.ConsoleHandler ();

        public static void Main ( string [] args )
        {
            Application.Cleanup ();

            Executor.InstallJobs ();

            ConsoleHandler.Install ();
            ConsoleHandler.Run ();
        }

        public static async Task RunCommand ( string line )
        {
            var commandSplit = line.Split ( ' ' );
            var commandName = commandSplit [ 0 ].ToLower ().Trim ();
            var command = ConsoleHandler.CommandHandler.Commands.Find ( x => x.FullName == commandName || x.Subname == commandName );
            if ( command == null )
            {
                Console.WriteLine ( $"Command \"{commandName}\" does not exist." );
                Console.WriteLine ();
                return;
            }

            try
            {
                command.Action?.Invoke ();
                command.ObjectAction?.Invoke ( commandSplit.ToString ( 1 ) );
            } catch ( Exception exception )
            {
                Console.WriteLine ( $"  Error: {exception.ToString ()}" );
            }

            await Task.CompletedTask;
        }
    }
}

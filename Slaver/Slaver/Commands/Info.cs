﻿using Humanlights;
using Slaver.Services;

namespace Slaver.Commands
{
    [Factory]
    public class Info : CommandLibrary
    {
        [Command ( Subname = "jobs", Help = "Prints all loaded jobs and few informations about their runtime behaviour." )]
        public static string PrintJobs ()
        {
            var jobs = "Current Jobs:\n";

            foreach ( var job in Executor.Jobs )
            {
                jobs += $"{job.ID}, {job.Name}, {job.Description}, {job.WorkspaceFolder}, {job.Stages.Length}, skip errors {job.SkipErrors}\n";
            }

            return jobs.Trim ();
        }
    }
}

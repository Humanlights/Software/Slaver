﻿using System.Collections.Generic;

namespace Slaver.Components
{
    [System.Serializable]
    public class Stage
    {
        public string Name { get; set; }
        public IList<string> Lines { get; set; } = new List<string> (); // Command-Line Argument lines - will use System.Diagnostics.Process to execute it.
    }
}
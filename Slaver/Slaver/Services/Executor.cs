﻿using Humanlights.Extensions;
using Slaver.Commands;
using Slaver.Components;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Slaver.Services
{
    public class Executor
    {
        public static IList<Job> Jobs { get; set; } = new List<Job> ();
        public static IList<Components.Runtime> RuntimeJobs { get; set; } = new List<Components.Runtime> ();

        public static Random Randomizer { get; set; } = new Random ();

        public static void InstallJobs ()
        {
            var files = Application.JobFiles;

            foreach ( var file in files )
            {
                var job = new Job ();
                {
                    var lines = OsEx.File.ReadTextLines ( file );

                    job.ID = Application.GetValue ( lines, "id" );
                    job.Name = Application.GetValue ( lines, "name" );
                    job.Description = Application.GetValue ( lines, "description" );
                    job.BuildNumber = Application.GetBuildNumber ( job.ID );

                    var workspace = Application.GetValue ( lines, "workspace" );
                    workspace = string.IsNullOrEmpty ( workspace ) ? $"{Application.WorkspacesFolder}\\{job.ID}" : workspace;
                    OsEx.Folder.Create ( workspace );
                    job.WorkspaceFolder = workspace;

                    var build = $"{Application.BuildsFolder}\\{job.ID}";
                    OsEx.Folder.Create ( build );
                    job.BuildFolder = build;

                    var artifact = $"{Application.ArtifactsFolder}\\{job.ID}\\build#{job.BuildNumber}";
                    OsEx.Folder.Create ( artifact );
                    job.ArtifactFolder = artifact;

                    job.SkipErrors = StringEx.ToBool ( Application.GetValue ( lines, "skiperrors" ) );
                    job.Stages = Application.GetStages ( lines );
                }
                Jobs.Add ( job );
            }
        }

        public static async Task<Process> ExecuteBat ( string line )
        {
            line = line.TrimStart ( '-' );

            var tempFile = System.IO.Path.Combine ( Application.TempFolder, $"{RandomEx.GetRandomString ( 10 )}.bat" );
            OsEx.File.Create ( tempFile, line );

            var process = new Process ();
            process.StartInfo.FileName = tempFile;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.WorkingDirectory = RuntimeConfigs.Job.WorkspaceFolder;

            //
            // Hide the process
            //
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.CreateNoWindow = true;
            }

            process.Start ();
            process.WaitForExit ();

            await Task.CompletedTask;
            return process;
        }
        public static async Task ExecuteJob ( string id )
        {
            var job = GetJob ( id );
            if ( job == null )
            {
                Console.WriteLine ( $"Job execution failed right before starting up" );
                return;
            }

            var buildNumber = Application.AppendBuildNumber ( job.ID );

            var runtimeJob = new Components.Runtime ( job.ID );
            RuntimeJobs.Add ( runtimeJob );

            Console.WriteLine ( $"Started Job -> {job.Name}" );
            Console.WriteLine ( $"Build Number: {buildNumber}" );
            Console.WriteLine ( $"  Deleting previous build (if any)..." );
            {
                OsEx.Folder.DeleteContents ( job.BuildFolder );
            }

            var stageCount = 0;
            foreach ( var stage in job.Stages )
            {
                stageCount++;

                if ( !string.IsNullOrEmpty ( stage.Name ) ) Console.WriteLine ( $" {stageCount.ToString ( "00" )} -> {stage.Name}" );

                for ( int i = 0; i < stage.Lines.Count; i++ )
                {
                    var line = stage.Lines [ i ];

                    try
                    {
                        // Console.WriteLine ( $"   -> {line.Truncate ( 100, "..." )}" );

                        RuntimeConfigs.Value = line.Split ( ' ' ).ToString ( 1 );
                        RuntimeConfigs.Job = job;
                        await Program.RunCommand ( line );
                        await Task.Delay ( Commands.Runtime.TimeoutMiliseconds );
                    }
                    catch ( Exception exception )
                    {
                        Console.WriteLine ( $"   -Failed>: {line.Truncate ( 100, "..." )}" );
                        Console.WriteLine ( $"              {exception.ToString ()}" );
                    }
                }

                if ( !string.IsNullOrEmpty ( stage.Name ) ) Console.WriteLine ( $"    -> {stage.Name} done." );
            }

            Console.WriteLine ( $"Finished Job: {job.Name}!" );

            job.BuildNumber = Application.GetBuildNumber ( job.ID );
            RuntimeJobs.Remove ( runtimeJob );
        }
        public static async Task StopJob ( string id )
        {
            var runtimeJob = RuntimeJobs.FirstOrDefault ( x => x.JobId == id );
            if ( runtimeJob == null )
            {
                Console.WriteLine ( $"Job \"{id}\" is not running or doesn't exist." );
                return;
            }

            runtimeJob.Process?.Kill ();
            runtimeJob.Process?.Close ();
            runtimeJob.Process?.Dispose ();

            await Task.CompletedTask;
        }

        public static Job GetJob ( string id )
        {
            var job = Jobs.FirstOrDefault ( x => x.ID == id );

            if ( job == null )
            {
                Console.WriteLine ( $"There is no job with ID {id}" );
                return null;
            }

            return job;
        }

        public static Components.Runtime GetRuntimeJob ( string id )
        {
            var job = RuntimeJobs.FirstOrDefault ( x => x.JobId == id );

            if ( job == null )
            {
                Console.WriteLine ( $"There is no job that's being executed with ID {id}" );
                return null;
            }

            return job;
        }
    }
}